/**
 * \file
 *
 * \brief CDC Application Main functions
 *
 * Copyright (c) 2011-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include <asf.h>
#include "conf_usb.h"
#include "ui.h"
#include "uart.h"
#include <string.h>
#include <math.h>
//#include "queue.hpp"
#include "queue.h"
static volatile bool main_b_cdc_enable = false;

//#define CONF_I2C_MASTER_MODULE SERCOM2

#define CONFIG_INTERRUPT_FORCE_INTC


/** PWM module to use */
#define CONF_PWM_MODULE      EXT2_PWM4CTRL_MODULE
/** PWM channel */
#define CONF_PWM_CHANNEL     EXT2_PWM4CTRL_0_CHANNEL
/** PWM output */
#define CONF_PWM_OUTPUT      EXT2_PWM4CTRL_0_OUTPUT
/** PWM output pin */
#define CONF_PWM_OUT_PIN     EXT2_PWM4CTRL_0_PIN
/** PWM output pinmux */
#define CONF_PWM_OUT_MUX     EXT2_PWM4CTRL_0_MUX

#define PWM_TOP 479
#define PWM_MATCH 0

#define MOTOR_DIR PIN_PB07

#define MOTOR_LOCK EXT2_PIN_IRQ

#define BUF_LENGTH 2
#define SLAVE_SELECT_1 PIN_PB05
#define SLAVE_SELECT_2 PIN_PB15
static uint8_t buffer[BUF_LENGTH] = {
	0x00, 0x04
};

static uint8_t buffer1[BUF_LENGTH] = {
	0x01, 0x19
};


//foo blah;
CommandMessage command_current;


NodeManager nm;
struct spi_module spi_master_instance;
struct spi_slave_inst slave;

static void configure_tcc(void);


struct tcc_module tcc_instance;


bool dir = 0;


struct port_config motor_dir_pin;

struct tc_module tc_instance_enc;
struct tc_module tc_instance_con;

bool motor_locked = false;
uint32_t x = 0;
double pos_a = 0.0;//10*3.9*512;
double pos_d =0.0;//-6.0*75.0/19.0*512.0;
double er = 0;
double prev_error=0;
double cmd = 0;
double curr_time = 0;
double prev_time = 0;

// PID values
#define KP_POS 2.0
#define KD_POS 160000.0
#define KI_POS 0.2

#define KP_TRAJ 0.001
#define KD_TRAJ 160000.0
#define KI_TRAJ 0.2

double kp = 1.0;//0.4;//0.0297;
double kd = 160000.0;//0.047;//14.53822;//926.424;//276.54;
double ki = 0.2;//0.0000163;//0;//0.006062;//0;//0.0000000007934;//0.00000000023684;

double integral=0;
double derivative=0;
double bias = 0;
double iteration_time = 1.0;
double ss_threshold = 180.0;
double limits[2] = {202000.0, -202000.0};

#define CONTROL 1
#define PWM 2
#define LIMITS 3


CommandMessage pos_cmd;

uint32_t pos_check = 0;
uint8_t motor_state = 1;
//CommandMessage commands[8] = {};
	
#define ERROR_FLAG 0
#define CALIB_ROTOR_LOCK_FLAG 1
#define ROTOR_LOCK_FLAG 2
#define HARD_STOP_FLAG 3
#define SOFT_STOP_FLAG 4


#define CONTROLLER1 0
#define CONTROLLER2 1
#define PWM_CONTROLLER 0
#define POSITION_CONTROLLER 1
#define TRAJECTORY_CONTROLLER 2
/**
flags
----------------
0 - error
1 - rotor locked
2 - hard stop
3 - soft stop
**/
bool flags[4] = {0, 0, 0, 0};
/*
controller commander
----------------
0 - no controller
1 - position
2 - trajectory
*/
uint8_t controller_commander[2] = {0, 0}; 
CommandMessage upper_limit_cmd;
CommandMessage lower_limit_cmd;
CommandMessage calibration_commands[2];
bool curr_command_status = 1;

char str1[8];
char str2[8];
//char str3[8];
void configure_extint_channel(void)
{
	//! [setup_1]
	struct extint_chan_conf config_extint_chan;
	//! [setup_1]
	//! [setup_2]
	extint_chan_get_config_defaults(&config_extint_chan);
	//! [setup_2]

	//! [setup_3]
	config_extint_chan.gpio_pin           = EXT1_IRQ_PIN;
	config_extint_chan.gpio_pin_mux       = EXT1_IRQ_MUX;
	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_UP;
	config_extint_chan.detection_criteria = EXTINT_DETECT_BOTH;
	//! [setup_3]
	//! [setup_4]
	extint_chan_set_config(EXT1_IRQ_INPUT, &config_extint_chan);
	//! [setup_4]
}



//! [setup_7]
void extint_detection_callback(void)
{
	if (!port_pin_get_input_level(EXT1_IRQ_PIN)) {
		flags[CALIB_ROTOR_LOCK_FLAG] = 1;
		flags[ROTOR_LOCK_FLAG] = 1;
	} else {
		flags[ROTOR_LOCK_FLAG] = 0;
	}
	//flags[1] != port_pin_get_input_level(EXT1_IRQ_PIN);

}

void configure_extint_callbacks(void)
{
	//! [setup_5]
	extint_register_callback(extint_detection_callback,
	EXT1_IRQ_INPUT,
	EXTINT_CALLBACK_TYPE_DETECT);
	//! [setup_5]
	//! [setup_6]
	extint_chan_enable_callback(EXT1_IRQ_INPUT,
	EXTINT_CALLBACK_TYPE_DETECT);
	//! [setup_6]
}

void configure_spi_master(void)
{
	//! [config]
	struct spi_config config_spi_master;
	//! [config]
	//! [slave_config]
	struct spi_slave_inst_config slave_dev_config;
	//! [slave_config]
	/* Configure and initialize software device instance of peripheral slave */
	//! [slave_conf_defaults]
	spi_slave_inst_get_config_defaults(&slave_dev_config);
	//! [slave_conf_defaults]
	//! [ss_pin]
	slave_dev_config.ss_pin = CONF_MASTER_SS_PIN;
	//! [ss_pin]
	//! [slave_init]
	spi_attach_slave(&slave, &slave_dev_config);
	//! [slave_init]
	/* Configure, initialize and enable SERCOM SPI module */
	//! [conf_defaults]
	spi_get_config_defaults(&config_spi_master);
	//! [conf_defaults]
	//! [mux_setting]
	config_spi_master.mux_setting = CONF_MASTER_MUX_SETTING;
	config_spi_master.transfer_mode = SPI_TRANSFER_MODE_0;
	//! [mux_setting]
	config_spi_master.generator_source = GCLK_GENERATOR_0;
	config_spi_master.mode_specific.master.baudrate = 1000000;
	config_spi_master.pinmux_pad0 = CONF_MASTER_PINMUX_PAD0; // miso 4
	config_spi_master.pinmux_pad1 = CONF_MASTER_PINMUX_PAD1; //ss 5
	config_spi_master.pinmux_pad2 = CONF_MASTER_PINMUX_PAD2; //mosi 6
	config_spi_master.pinmux_pad3 = CONF_MASTER_PINMUX_PAD3; //sck 7

	//! [init]
	spi_init(&spi_master_instance, CONF_MASTER_SPI_MODULE, &config_spi_master);
	//! [init]

	//! [enable]
	spi_enable(&spi_master_instance);
	//! [enable]

}


static void tcc_callback_to_change_duty_cycle(
struct tcc_module *const module_inst)
{

}
uint16_t diff = 0;
int vald = 0;
uint16_t rx_data[7] = {};
//uint16_t rx_data1 = 0;
//uint16_t rx_data2 = 0;
//uint16_t rx_data3 = 0;
//uint16_t rx_data4 = 0;
//uint16_t rx_data5 = 0;
//uint16_t rx_data6 = 0;
bool spi_setup = 0;
double traj_val = 1000.0;
uint32_t cm = 0;
bool prev_dir = 1;
bool posdcheck = 0;
uint8_t cmd_count = 0;
uint8_t traj_count = 0;
double traj_dir = 0;
int lock_flag = 0;
CommandMessage command;
void TC4_IRQHandler(struct tc_module *const module_inst)
{
	
	if (spi_setup == 1) {
		//while(curr_command_status) {
			spi_select_slave(&spi_master_instance, &slave, true);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[0]);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[1]);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[2]);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[3]);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[4]);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[5]);
			spi_transceive_wait(&spi_master_instance, 0x88, &rx_data[6]);
			spi_select_slave(&spi_master_instance, &slave, false);
			x = (rx_data[1]<< 24) + (rx_data[2]<< 16) + (rx_data[3] << 8) + rx_data[4];
	
			pos_a = (double)((int32_t) x);
			if (pos_d > limits[0] || pos_d < limits[1]) {
				flags[0] = 1;
			} else {
				flags[0] = 0;
			}
		
			if (curr_command_status) {
				if (nm.size > 0) {
					command_current = deQueue(&nm);
					curr_command_status = 0;
				} else {
					command_current.commandType = 0x01;
					//command_current = pos_cmd;
				}
				
			}
			if (command_current.commandType != NULL) {

				switch (command_current.commandType) {
					case(0x01 || 0x00):
						controller_commander[CONTROLLER1] = PWM_CONTROLLER;
						cmd = 0;
						//flags[0] = 1;
						//if (nm.size > 0) {
						//curr_command_status = 1;
							////flags[0] = 0;
						//}
						break;
					case (0x11): // PWM
						cmd = command_current.cmd;
						controller_commander[CONTROLLER1] = PWM_CONTROLLER;
						if (flags[CALIB_ROTOR_LOCK_FLAG]) {
							curr_command_status = 1;
							flags[CALIB_ROTOR_LOCK_FLAG] = 0;
							//cmd = 0;
						}
						break;
					case (0x12): // pos controller
						pos_d = command_current.cmd/100.0*(limits[1] - limits[0]) + limits[0];
						controller_commander[CONTROLLER1] = POSITION_CONTROLLER;
						if (posdcheck) {
							curr_command_status = 1;
						}
						break;
					case (0x13):
						controller_commander[CONTROLLER1] = TRAJECTORY_CONTROLLER;
						if (traj_count == 0) {
							pos_d = pos_a;
							traj_val = (command_current.cmd - pos_a)/100.0;
							if (pos_d > pos_a) {
								traj_dir = 1.0;
							} else {
								traj_dir = -1.0;
							}
						} else if (traj_count >= 100) {
							traj_count = 0;
							curr_command_status = 1;
							break;
						}
						if (posdcheck) {
							pos_d -= traj_val*(traj_dir);
							traj_count +=1;
						}
						break;
					case (0x80):
						//controller_commander[CONTROLLER1] = PWM_CONTROLLER;
						cmd = calibration_commands[cmd_count].cmd;
						controller_commander[CONTROLLER1] = PWM_CONTROLLER;
						if (flags[CALIB_ROTOR_LOCK_FLAG]) {
							lock_flag+= 1;
							if (lock_flag > 10000) {
								limits[cmd_count] = pos_a;
								cmd_count +=1;
								lock_flag = 0;
								flags[CALIB_ROTOR_LOCK_FLAG] = 0;
							}
							
							//cmd = 0;
							
						} else {
							lock_flag = 0;
						}
						if (cmd_count > 1) {
							curr_command_status = 1;
							//traj_val = (limits[1] - limits[0])/10.0;
							cmd_count = 0;
						}
						break;
				}
			}
		//}
		er = pos_d - pos_a;
		if (abs(er) < ss_threshold) {

			pos_check += 1;
			if (pos_check > 20000) {
				//if (posdcheck == 1) {
					//if (flags[1]) {
						//pos_d = 4.0*75.0/19.0*512.0;
					//}
					//posdcheck = 0;
				//} else {
					////pos_d = -6.0*75.0/19.0*512.0;
					//posdcheck = 1;
				//}
				posdcheck = 1;
				pos_check = 0;
				integral = 0;
			}

		} else {
			posdcheck = 0;
			pos_check = 0;
		}
		curr_time = (double) tc_get_count_value(&tc_instance_con);
		iteration_time = curr_time - prev_time + 19199.0;
		integral = integral + er*iteration_time;
		if (integral > 1000.0) {
			integral = 1000.0;
		} else if (integral < -1000.0) {
			integral = -1000.0;
		}
		derivative = (er - prev_error)/iteration_time;
		switch(controller_commander[CONTROLLER1]) {
			case (POSITION_CONTROLLER):
				kp = KP_POS;
				kd = 0;//KD_POS;
				ki =0;// KI_POS;
				cmd = kp*er + ki*integral + kd*derivative + bias;
				break;
			case (TRAJECTORY_CONTROLLER):
				kp = KP_TRAJ;
				kd = KD_TRAJ;
				ki = KI_TRAJ;
				cmd = kp*er + ki*integral + kd*derivative + bias;
				break;
			case (PWM_CONTROLLER):
				break;
		}
		//if (controller_commander[CONTROLLER1] != PWM_CONTROLLER) {
			//cmd = kp*er + ki*integral + kd*derivative + bias;
		//}
		
		
		if (cmd < 0) {
			dir = 0;
		} else {
			dir = 1;
		}
		cmd = (uint32_t) ceil(abs(cmd));
		
		if (cmd >238.0) {
			cmd = 238.0;
		} 
		
		port_pin_set_output_level(MOTOR_DIR, dir);
//
		//if (flags[0]) {
			//tcc_set_compare_value(&tcc_instance, (enum tcc_match_capture_channel)
			//(TCC_MATCH_CAPTURE_CHANNEL_0 + CONF_PWM_CHANNEL), 0);
		//} else {
			tcc_set_compare_value(&tcc_instance, (enum tcc_match_capture_channel)
			(TCC_MATCH_CAPTURE_CHANNEL_0 + CONF_PWM_CHANNEL), (uint32_t) cmd);
		//}
		
		
		prev_dir = dir;
		prev_error = er;
		prev_time = curr_time;
		
	}
}



void configure_tc_con(void)
{
	//! [setup_config]
	struct tc_config config_tc;
	//! [setup_config]
	//! [setup_config_defaults]
	tc_get_config_defaults(&config_tc);
	//! [setup_config_defaults]
	
	//! [setup_change_config]
	config_tc.wave_generation = TC_WAVE_GENERATION_MATCH_FREQ;
	config_tc.counter_16_bit.compare_capture_channel[0] = 0; // control
	//! [setup_change_config]
	
	
	//! [setup_set_config]
	tc_init(&tc_instance_con, TC4, &config_tc);
	//! [setup_set_config]

	//! [setup_enable]
	tc_enable(&tc_instance_con);
	//! [setup_enable]
	tc_set_top_value(&tc_instance_con, 19199);//5kHz//10kHz // 9599 5kHz
	
}

void configure_tc_spi(void)
{
	//! [setup_config]
	struct tc_config config_tc;
	//! [setup_config]
	//! [setup_config_defaults]
	tc_get_config_defaults(&config_tc);
	//! [setup_config_defaults]
	
	//! [setup_change_config]
	config_tc.wave_generation = TC_WAVE_GENERATION_MATCH_FREQ;
	config_tc.counter_16_bit.compare_capture_channel[0] = 0; // control
	//! [setup_change_config]
	
	
	//! [setup_set_config]
	tc_init(&tc_instance_con, TC4, &config_tc);
	//! [setup_set_config]

	//! [setup_enable]
	tc_enable(&tc_instance_con);
	//! [setup_enable]
	tc_set_top_value(&tc_instance_con, 9599);//5kHz//10kHz // 9599 5kHz
	
}



void configure_tc_callbacks(void)
{
	//! [setup_register_callback]

	tc_register_callback(
	&tc_instance_con,
	TC4_IRQHandler,
	TC_CALLBACK_CC_CHANNEL0);

	tc_enable_callback(&tc_instance_con, TC_CALLBACK_CC_CHANNEL0);
	
	
}

static void configure_tcc(void)
{
	//! [setup_config]
	struct tcc_config config_tcc;

	//! [setup_config]
	//! [setup_config_defaults]
	tcc_get_config_defaults(&config_tcc, CONF_PWM_MODULE);
	//! [setup_config_defaults]

	//! [setup_change_config]
	config_tcc.counter.period = PWM_TOP; //100kHz//479//0xFFFF;
	//config_tcc.counter.clock_source = GCLK_GENERATOR_0; //48M
	//config_tcc.counter.clock_prescaler = TCC_CLOCK_PRESCALER_DIV1;
	
	config_tcc.compare.wave_generation = TCC_WAVE_GENERATION_SINGLE_SLOPE_PWM;
	config_tcc.compare.match[CONF_PWM_CHANNEL] = 0;//PWM_TOP;//0xFFFF;
	//! [setup_change_config]

	//! [setup_change_config_pwm]
	config_tcc.pins.enable_wave_out_pin[CONF_PWM_OUTPUT] = true;
	config_tcc.pins.wave_out_pin[CONF_PWM_OUTPUT]        = CONF_PWM_OUT_PIN;
	config_tcc.pins.wave_out_pin_mux[CONF_PWM_OUTPUT]    = CONF_PWM_OUT_MUX;
	//! [setup_change_config_pwm]

	//tcc_set_top_value(&tcc_instance, 479); // 100kHz
	//! [setup_set_config]
	tcc_init(&tcc_instance, CONF_PWM_MODULE, &config_tcc);
	//! [setup_set_config]

	//! [setup_enable]
	tcc_enable(&tcc_instance);
	//! [setup_enable]
}

static void configure_tcc_callbacks(void)
{
	//! [setup_register_callback]
	tcc_register_callback(
	&tcc_instance,
	tcc_callback_to_change_duty_cycle,
	(enum tcc_callback)(TCC_CALLBACK_CHANNEL_0 + CONF_PWM_CHANNEL));
	//! [setup_register_callback]

	//! [setup_enable_callback]
	tcc_enable_callback(&tcc_instance,
	(enum tcc_callback)(TCC_CALLBACK_CHANNEL_0 + CONF_PWM_CHANNEL));
	//! [setup_enable_callback]
	
}








/*! \brief Main function. Execution starts here.
 */
int main(void)
{
	
	irq_initialize_vectors();
	//cpu_irq_enable();
	// Initialize the sleep manager
	sleepmgr_init();
	
	system_init();
	ui_init();
	ui_powerdown();
	
	// Start USB stack to authorize VBus monitoring
	udc_start();
	
	configure_spi_master();


	motor_dir_pin.direction = PORT_PIN_DIR_OUTPUT;
	motor_dir_pin.input_pull = PORT_PIN_PULL_NONE;
	port_pin_set_config(MOTOR_DIR, &motor_dir_pin);


	
	delay_init();
	configure_tcc();

	configure_tc_con();
	configure_tc_callbacks();

	configure_extint_channel();
	configure_extint_callbacks();


	system_interrupt_set_priority(SYSTEM_INTERRUPT_MODULE_EIC, SYSTEM_INTERRUPT_PRIORITY_LEVEL_1);

	system_interrupt_set_priority(SYSTEM_INTERRUPT_MODULE_TC4, SYSTEM_INTERRUPT_PRIORITY_LEVEL_2);
	system_interrupt_set_priority(SYSTEM_INTERRUPT_MODULE_USB, SYSTEM_INTERRUPT_PRIORITY_LEVEL_3);
	nm = initNodeManager();
	upper_limit_cmd.commandType = 0x11;
	upper_limit_cmd.cmd = 450.0;
	lower_limit_cmd.commandType = 0x11;
	lower_limit_cmd.cmd = -450.0;
	calibration_commands[0] = upper_limit_cmd;
	calibration_commands[1] = lower_limit_cmd;
	controller_commander[CONTROLLER1] = PWM_CONTROLLER;
	cmd = 0;
	pos_cmd.commandType = 0x80;
	pos_cmd.cmd = 238.0;
	system_interrupt_enable_global();
	//while(1) {
		//delay_s(5);
	//}
	//QueueInit();
	//command_current.commandType = 0x12;
	//command_current.cmd = 512.0*3.9*10.0;
	//command1.commandType = 0x11;
	//command1.cmd = 95.0;
	//command2.commandType = 0x11;
	//command2.cmd = 150.0;
	//command3.commandType = 0x11;
	//command3.cmd = 50.0;
	//
	//CommandMessage commandList[3] = {command1, command2, command3};
	
	
	//for (int i = 0; i < 3; i++) {
		//enQueue(&nm, commandList[i]);
	//}
	
	
	//Node test = initNode(command_current);
	//enQueue(&nm, command_current);
	
	//enQueue(&nm, command3);
	//CommandMessage cmdtest = peekQueue(&nm);//deQueue(&nm);
	//CommandMessage cmdtest = deQueue(&nm);
	//cmdtest = deQueue(&nm);
	//cmdtest = deQueue(&nm);
	//cmdtest = deQueue(&nm);
	int l = 0;
	spi_select_slave(&spi_master_instance, &slave, true);
	spi_write_buffer_wait(&spi_master_instance, buffer, BUF_LENGTH);
	spi_select_slave(&spi_master_instance, &slave, false);
	spi_select_slave(&spi_master_instance, &slave, true);
	spi_write_buffer_wait(&spi_master_instance, buffer1, BUF_LENGTH);
	spi_select_slave(&spi_master_instance, &slave, false);
	spi_setup = 1;
	int32_t c = 0;
	//enQueue(&nm, pos_cmd);
	while(1) {
		if (udi_cdc_is_rx_ready()) {
			uint32_t num = udi_cdc_get_nb_received_data();
			char str2[8];
			udi_cdc_read_buf(&str2, num);
			
			CommandMessage new_cmd;
			new_cmd.commandType = str2[0];//((str2[0] - 30) << 1) + (str2[1]-30);
			if (num > 0) {
				c = (int32_t)((str2[1]<<24) + (str2[2]<<16) + (str2[3]<<8) + str2[4]);
				new_cmd.cmd = (double) c;//((str2[2] - 30) << 1) + (str2[3]-30);
			}
			enQueue(&nm, new_cmd);
			//CommandMessage cmd_msg = deQueue(&nm);
			//char str3[8];
			//sprintf(str3, "%c\r\n", (cmd_msg.commandType));
			//udi_cdc_write_buf(str3, 1);
		}
		
		//udi_cdc_read_buf(str1, 16);
		//udi_cdc_read_buf(&str2, 16);
		
	
		//iram_size_t 
		//if (str2[0] != str3[0]) {
			//sprintf(str2, "%i\r\n", (int)(cmd));
			//delay_s(1);
		
			//str2 = str3;
		//}
		
		//for (int i = 0; i < sizeof(commands); i++) {
			//QueuePut(commands[i]);
		//}
		
		if (udi_cdc_is_tx_ready()) {
			char str3[8];
			//if (flags[1] == 1) {
				sprintf(str3, "%c\r\n", (char)cmd);
				udi_cdc_write_buf(str3, 1);
			//}
			
		}
	}


}



void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_b_cdc_enable)
		return;
	ui_process(udd_get_frame_number());
}

#ifdef USB_DEVICE_LPM_SUPPORT
void main_suspend_lpm_action(void)
{
	ui_powerdown();
}

void main_remotewakeup_lpm_disable(void)
{
	ui_wakeup_disable();
}

void main_remotewakeup_lpm_enable(void)
{
	ui_wakeup_enable();
}
#endif

bool main_cdc_enable(uint8_t port)
{
	main_b_cdc_enable = true;
	// Open communication
	uart_open(port);
	return true;
}

void main_cdc_disable(uint8_t port)
{
	main_b_cdc_enable = false;
	// Close communication
	uart_close(port);
}

void main_cdc_set_dtr(uint8_t port, bool b_enable)
{
	if (b_enable) {
		// Host terminal has open COM
		ui_com_open(port);
	}else{
		// Host terminal has close COM
		ui_com_close(port);
	}
}

/**
 * \mainpage ASF USB Device CDC
 *
 * \section intro Introduction
 * This example shows how to implement a USB Device CDC
 * on Atmel MCU with USB module.
 * The application note AVR4907 http://ww1.microchip.com/downloads/en/appnotes/doc8447.pdf
 * provides more information about this implementation.
 *
 * \section desc Description of the Communication Device Class (CDC)
 * The Communication Device Class (CDC) is a general-purpose way to enable all
 * types of communications on the Universal Serial Bus (USB).
 * This class makes it possible to connect communication devices such as
 * digital telephones or analog modems, as well as networking devices
 * like ADSL or Cable modems.
 * While a CDC device enables the implementation of quite complex devices,
 * it can also be used as a very simple method for communication on the USB.
 * For example, a CDC device can appear as a virtual COM port, which greatly
 * simplifies application development on the host side.
 *
 * \section startup Startup
 * The example is a bridge between a USART from the main MCU
 * and the USB CDC interface.
 *
 * In this example, we will use a PC as a USB host:
 * it connects to the USB and to the USART board connector.
 * - Connect the USART peripheral to the USART interface of the board.
 * - Connect the application to a USB host (e.g. a PC)
 *   with a mini-B (embedded side) to A (PC host side) cable.
 * The application will behave as a virtual COM (see Windows Device Manager).
 * - Open a HyperTerminal on both COM ports (RS232 and Virtual COM)
 * - Select the same configuration for both COM ports up to 115200 baud.
 * - Type a character in one HyperTerminal and it will echo in the other.
 *
 * \note
 * On the first connection of the board on the PC,
 * the operating system will detect a new peripheral:
 * - This will open a new hardware installation window.
 * - Choose "No, not this time" to connect to Windows Update for this installation
 * - click "Next"
 * - When requested by Windows for a driver INF file, select the
 *   atmel_devices_cdc.inf file in the directory indicated in the Atmel Studio
 *   "Solution Explorer" window.
 * - click "Next"
 *
 * \copydoc UI
 *
 * \section example About example
 *
 * The example uses the following module groups:
 * - Basic modules:
 *   Startup, board, clock, interrupt, power management
 * - USB Device stack and CDC modules:
 *   <br>services/usb/
 *   <br>services/usb/udc/
 *   <br>services/usb/class/cdc/
 * - Specific implementation:
 *    - main.c,
 *      <br>initializes clock
 *      <br>initializes interrupt
 *      <br>manages UI
 *      <br>
 *    - uart_xmega.c,
 *      <br>implementation of RS232 bridge for XMEGA parts
 *    - uart_uc3.c,
 *      <br>implementation of RS232 bridge for UC3 parts
 *    - uart_sam.c,
 *      <br>implementation of RS232 bridge for SAM parts
 *    - specific implementation for each target "./examples/product_board/":
 *       - conf_foo.h   configuration of each module
 *       - ui.c        implement of user's interface (leds,buttons...)
 */
