/*
 * queue.h
 *
 * Created: 13/02/2019 12:48:24 PM
 *  Author: mitch
 */ 


#ifndef QUEUE_H_
#define QUEUE_H_
//#include <>
typedef struct _CommandMessage {
	/*
	Z axis only
	commandType
		hard stop	- 0x00		// immediately stop the current cmd
		soft stop	- 0x01		// stop after current cmd
		pwm			- 0x11
		pos cmd		- 0x12
		traj cmd	- 0x13		// initial pos cmd, with predetermined intervals
		calibrate	- 0x80		// pwm to each limit to set them, pos cmd back to neutral pos
		pickup tip	- 0x81		// pos cmd to just above the tips, pwm to pickup, pos cmd to neutral pos
		get liquid	- 0x82		// pos cmd to the wells, traj cmd into wells, pos cmd to neutral
		dispense	- 0x83		// pos cmd to just above the wells, pos cmd to neutral
		
		error		- 0xFF
	cmd
		hard stop	- n/a
		soft stop	- n/a
		pwm			- -479.0 to 479.0
		pos cmd		- lower_limit to higher_limit
		traj cmd	- lower_limit to higher_limit
		calibrate	- n/a
		pickup tip	- n/a
		get liquid	- n/a
		dispense	- n/a
	*/
	uint8_t commandType;
	double cmd;
	
}CommandMessage;


typedef struct _Node {
	
	struct _Node * next;
	CommandMessage cmd;
	
	
	}Node;

typedef struct _NodeManager {
	Node *head;
	Node *tail;
	uint8_t size;
	
}NodeManager;

NodeManager initNodeManager(void);

Node* initNode(CommandMessage command);
uint8_t getSize(NodeManager *nm);
void enQueue(NodeManager* nm, CommandMessage command);

CommandMessage deQueue(NodeManager *nm);
CommandMessage peekQueue(NodeManager *nm);
#endif /* QUEUE_H_ */