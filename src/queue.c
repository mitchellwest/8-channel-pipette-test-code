/*
 * queue.c
 *
 * Created: 13/02/2019 12:44:05 PM
 *  Author: mitch
 */ 

#include <asf.h>
#include <stdio.h>
#include <stdlib.h>
#define QUEUE_SIZE 1024
#include "queue.h"
//#include <iostream>

//struct foo {
	//
//};

	
// private
Node * initNode(CommandMessage command) {
	//Node n = {NULL, command};
	Node *nodePtr = (Node *) malloc(sizeof(Node));
	(*nodePtr).next = NULL;
	(*nodePtr).cmd = command;
	return nodePtr;
}


// public
NodeManager initNodeManager(void) {
	NodeManager nm;
	nm.head = NULL;
	nm.tail = NULL;
	nm.size = 0;
	return nm;
}

uint8_t getSize(NodeManager *nm) {
	return nm->size;
}

void enQueue(NodeManager *nm, CommandMessage command) {
	Node * new_tail = initNode(command);
	if ((*nm).head == NULL) {
		(*nm).head = new_tail;
		(*nm).tail = new_tail;
	} else {
		nm->tail->next = new_tail;
		nm->tail = new_tail;
	}
	
	nm->size+=1;
	//return new_tail;
}
//
CommandMessage peekQueue(NodeManager *nm) {
	return nm->head->cmd;
}


CommandMessage deQueue(NodeManager *nm) {
	CommandMessage cmd;
	if (nm->size > 0) {
		cmd = nm->head->cmd;
		(*nm).head = (*(*nm).head).next;
		free((*nm).head);
		nm->size--;	
	}
	else {
		cmd.commandType = 0x02;
	}
	return cmd;
}

