################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

src\ASF\sam0\drivers\dac\dac_sam_d_c_h\dac.c

src\ASF\sam0\drivers\dac\dac_sam_d_c_h\dac_callback.c

src\ASF\sam0\drivers\sercom\i2c\i2c_sam0\i2c_master.c

src\ASF\sam0\drivers\sercom\i2c\i2c_sam0\i2c_master_interrupt.c

src\ASF\sam0\drivers\sercom\spi\spi.c

src\ASF\sam0\drivers\sercom\spi\spi_interrupt.c

src\ASF\sam0\drivers\sercom\spi_master_vec\spi_master_vec.c

src\ASF\sam0\drivers\tcc\tcc.c

src\ASF\sam0\drivers\tcc\tcc_callback.c

src\ASF\sam0\drivers\tc\tc_interrupt.c

src\ASF\sam0\drivers\tc\tc_sam_d_r_h\tc.c

src\ASF\sam0\utils\stdio\read.c

src\ASF\sam0\utils\stdio\write.c

src\ASF\common2\services\delay\sam0\systick_counter.c

src\ASF\common\services\usb\class\cdc\device\example\samd21j18a_samd21_xplained_pro\ui.c

src\ASF\common\services\sleepmgr\samd\sleepmgr.c

src\ASF\common\services\usb\class\cdc\device\udi_cdc.c

src\ASF\common\services\usb\class\cdc\device\udi_cdc_desc.c

src\ASF\common\services\usb\udc\udc.c

src\ASF\common\utils\interrupt\interrupt_sam_nvic.c

src\ASF\sam0\boards\samd21_xplained_pro\board_init.c

src\ASF\sam0\drivers\extint\extint_callback.c

src\ASF\sam0\drivers\extint\extint_sam_d_r_h\extint.c

src\ASF\sam0\drivers\port\port.c

src\ASF\sam0\drivers\sercom\sercom.c

src\ASF\sam0\drivers\sercom\sercom_interrupt.c

src\ASF\sam0\drivers\sercom\usart\usart.c

src\ASF\sam0\drivers\sercom\usart\usart_interrupt.c

src\ASF\sam0\drivers\system\clock\clock_samd21_r21_da_ha1\clock.c

src\ASF\sam0\drivers\system\clock\clock_samd21_r21_da_ha1\gclk.c

src\ASF\sam0\drivers\system\interrupt\system_interrupt.c

src\ASF\sam0\drivers\system\pinmux\pinmux.c

src\ASF\sam0\drivers\system\system.c

src\ASF\sam0\drivers\usb\stack_interface\usb_device_udd.c

src\ASF\sam0\drivers\usb\stack_interface\usb_dual.c

src\ASF\sam0\drivers\usb\usb_sam_d_r\usb.c

src\ASF\sam0\utils\cmsis\samd21\source\gcc\startup_samd21.c

src\ASF\sam0\utils\cmsis\samd21\source\system_samd21.c

src\ASF\sam0\utils\syscalls\gcc\syscalls.c

src\main.c

src\queue.c

src\uart_samd.c

